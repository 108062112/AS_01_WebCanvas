# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Canvas Resize                                    | 1~5%      | Y         |


---

### How to use 

    使用直觀的設計，左方為控制面板，包含了：
    pen筆刷、era橡皮擦、clr全部清除、txt文字輸入、
    cir繪製圓形、rct繪製矩形、tri繪製三角形、lin繪製直線、
    udo復原、rdo取消復原、uld上傳圖片、dld下載畫布，
    共12項控制項，以及
    Canvas Size畫布尺寸調整、
    Thickness筆刷 / 圖形邊框粗度調整（滑桿搭配數字即時顯示）、
    Font文字輸入字型與大小調整、
    Color筆刷 / 圖形顏色調整。

    而右方則為畫布面板，
    其中若使用「上傳圖片」的功能，
    則該面板區域改為放置圖片（同時禁止更改畫布尺寸）。

### Function description

    switchTool：切換tool，並在左側面板icon與右側面板cursor作相對應變化。
    clearCanvas：清空canvas，並初始化canvasHistory與canvasHistoryStep。
    undo / redo：以陣列canvasHistory作為canvas的歷程記錄，並以canvasHistoryStep作為index。
    upload：使用FileReader()完成上傳效果。
    download：使用元素a完成下載效果。
    （Bonus）resize：更改canvas尺寸，在css中多加著墨了，在不同尺寸的情況下，仍維持整體版面水平置中、兩側版面垂直置中的設計。
    updateThickness：及時更新左側面板的thickness顯示數值。
    canvas.addEventListener：
        click：主要作為文字輸入工具的點擊偵測。
        mousedown：初始化ctx相關變數、記錄初始座標。
        mousemove：根據不同的tool繪製圖形，其中藉由歷程記錄搭配ctx.putImageData()完成拖曳效果。
        mouseup：歷程記錄相關變數的調整。

### Gitlab page link

    your web page URL, which should be "https://108062112.gitlab.io/AS_01_WebCanvas"

### Others (Optional)

    Anythinh you want to say to TAs.

<style>
table th{
    width: 100%;
}
</style>