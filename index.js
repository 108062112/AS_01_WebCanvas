//canvas DOM
const canvas = document.getElementById("canvas");
const ctx = canvas.getContext("2d"); //ctx
let canvasHistory = [];
let canvasHistoryStep = 0;

let headX = 0;
let headY = 0;
let lastX = 0;
let lastY = 0;
let currX = 0;
let currY = 0;

let isMouseActive = false;
let lastTool;
let currTool = "pen";
switchTool(currTool);

function switchTool(tool) {
    lastTool = currTool;
    currTool = tool;
    document.getElementById(lastTool).style.opacity = 0.6;
    document.getElementById(currTool).style.opacity = 1;
    canvas.style.cursor = "url('./img/cursor-" + tool + ".png'), auto";
}

function clearCanvas() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    canvasHistory = [];
    canvasHistoryStep = 0;
}

function undo() {
    if (canvasHistoryStep > 0)
        ctx.putImageData(canvasHistory[--canvasHistoryStep], 0, 0);
}

function redo() {
    if (canvasHistoryStep < canvasHistory.length - 1)
        ctx.putImageData(canvasHistory[++canvasHistoryStep], 0, 0);
}

function upload(imgInput) {
    var reader = new FileReader();
    reader.onload = function (e) {
        var img = document.getElementById("uploadedImg");
        img.setAttribute("src", e.target.result);
    }
    reader.readAsDataURL(imgInput.files[0]);
    document.getElementById("canvas").style.display = "none";
    document.getElementById("canvasW").disabled = true;
    document.getElementById("canvasH").disabled = true;
}

function download() {
    var img = canvas.toDataURL();

    var tmpLink = document.createElement('a');
    tmpLink.download = 'image.png';

    tmpLink.href = img;
    document.body.appendChild(tmpLink);
    tmpLink.click();
    document.body.removeChild(tmpLink);
}

function resize() {
    var w = document.getElementById("canvasW").value;
    var h = document.getElementById("canvasH").value;
    canvas.width = w;
    canvas.height = h;
}

function updateThickness(value) {
    var thicknessDisplay = document.getElementById("thicknessNum");
    thicknessDisplay.innerHTML = value;
}

canvas.addEventListener("click", function (e) {
    var lastTxtbox = document.getElementById("txtbox");
    if (lastTxtbox) {
        lastTxtbox.remove();
        return;
    }

    if (currTool === "txt") {
        var canvasPosition = canvas.getBoundingClientRect();
        var fontName = document.getElementById("fontList").value;
        var fontSize = document.getElementById("fontSize").value;
        var color = document.getElementById("colorPicker").value;

        var currTxtbox = document.createElement("input");
        currTxtbox.setAttribute("type", "text");
        currTxtbox.setAttribute("id", "txtbox");
        currTxtbox.style.position = "absolute";
        currTxtbox.style.left = (e.clientX - canvasPosition.x) + "px";
        currTxtbox.style.top = (e.clientY - canvasPosition.y) + "px";
        currTxtbox.style.background = "#9AC3C6";
        document.getElementById("canvasPanel").appendChild(currTxtbox);

        currTxtbox.addEventListener("blur", function () {
            ctx.font = "20pt Arial";
            ctx.fillStyle = color;
            //ctx.font = fontSize + "pt " + fontName;
            ctx.fillText(currTxtbox.value, e.clientX, e.clientY);
        })
    }
})

canvas.addEventListener("mousedown", function (e) {
    var thickness = document.getElementById("thicknessBar").value;
    var color = document.getElementById("colorPicker").value;
    isMouseActive = true;
    lastX = e.offsetX;
    lastY = e.offsetY;
    headX = lastX;
    headY = lastY;

    ctx.lineWidth = thickness;
    ctx.strokeStyle = color;
    ctx.lineCap = "round";
    ctx.lineJoin = "round";

    if (currTool === "era") ctx.globalCompositeOperation = "destination-out";
    else ctx.globalCompositeOperation = "source-over";
})

canvas.addEventListener("mousemove", function (e) {
    if (!isMouseActive) return;
    currX = e.offsetX;
    currY = e.offsetY;
    let left = headX < currX ? headX : currX;
    let top = headY < currY ? headY : currY;
    let w = Math.abs(headX - currX);
    let h = Math.abs(headY - currY);
    let r = w < h ? w * Math.pow(2, 0.5) / 2 : h * Math.pow(2, 0.5) / 2;

    switch (currTool) {
        case "pen": //pen
        case "era": //eraser
            ctx.beginPath();
            ctx.moveTo(lastX, lastY);
            ctx.lineTo(currX, currY);
            ctx.stroke();
            break;

        case "txt": //text
            break;

        case "cir": //circle
            ctx.putImageData(canvasHistory[canvasHistoryStep], 0, 0);
            ctx.beginPath();
            ctx.arc(left + w / 2, top + h / 2, r, 0, 2 * Math.PI);
            ctx.stroke();
            break;

        case "rct": //rectangle
            ctx.putImageData(canvasHistory[canvasHistoryStep], 0, 0);
            ctx.beginPath();
            ctx.rect(left, top, w, h);
            ctx.stroke();
            break;

        case "tri": //triangle
            ctx.putImageData(canvasHistory[canvasHistoryStep], 0, 0);
            ctx.beginPath();
            if (headY < currY) {
                ctx.moveTo(headX, currY);
                ctx.lineTo(currX, currY);
                ctx.lineTo(headX < currX ? currX - w / 2 : currX + w / 2, headY);
                ctx.lineTo(headX, currY);
            } else {
                ctx.moveTo(headX, headY);
                ctx.lineTo(currX, headY);
                ctx.lineTo(headX < currX ? currX - w / 2 : currX + w / 2, currY);
                ctx.lineTo(headX, headY);
            }
            ctx.stroke();
            break;

        case "lin": //line
            ctx.putImageData(canvasHistory[canvasHistoryStep], 0, 0);
            ctx.beginPath();
            ctx.moveTo(headX, headY);
            ctx.lineTo(currX, currY);
            ctx.stroke();
            break;
    }

    lastX = currX;
    lastY = currY;
})

canvas.addEventListener("mouseup", function (e) {
    if (canvasHistoryStep != canvasHistory.length - 1)
        canvasHistory.length = canvasHistoryStep + 1; // replace new step
    canvasHistory.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
    canvasHistoryStep++;
    isMouseActive = false;
})